//
// Created by Kiouche on 1/20/2020.
//

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "graph.h"
#include "hash.h"





namespace  std {

    vector<edge> get_edges(graph &g,bool directed){
        vector<edge> edges;
        unordered_set<edge> s_edges;
        for (auto & s : g){
            for (auto & d : s.second ){
                edge e,r_e;
                e.first = s.first;
                e.second = d;
                r_e.first = d;
                r_e.second = s.first;
                if(!directed) {
                    if (s_edges.find(e) == s_edges.end()) {
                        edges.push_back(e);
                    }
                }
                else    edges.push_back(e);
                s_edges.insert(e);
                s_edges.insert(r_e);
            }
        }
        return  edges;
    }

    bool is_it_undirected(graph &g){
        for (auto s : g){
            for (auto d : s.second ){
                if ( g[d].find(s.first)==g[d].end()) return  false;
            }
        }
        return  true;
    }

    unordered_set<uint32_t > intersection(unordered_set<uint32_t> &s1,unordered_set<uint32_t> &s2){
        unordered_set<uint32_t > intersection;
        if (s1.size()< s2.size()){
            for (auto n :s1){
                if (s2.find(n)!=s2.end()) intersection.insert(n);
            }
        }
        else{
            for (auto n :s2){
                if (s1.find(n)!=s1.end()) intersection.insert(n);
            }
        }
        return  intersection;
    }


}