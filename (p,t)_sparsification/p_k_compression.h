//
// Created by Kiouche on 1/20/2020.
//

#ifndef P_K_COMPRESSION_P_K_COMPRESSION_H
#define P_K_COMPRESSION_P_K_COMPRESSION_H
#include "graph.h"

namespace std {


    bool check_constraints(graph &original_graph,graph &compressed_graph,vector<double> p, int k);


    bool BFS(uint32_t node, graph &g,graph &constructed_graph,vector<double> &p,int &maxDepth);

    bool test_insert(edge &e, bool directed,graph &constructed_graph,graph &compressed_graph ,int k, vector<double> &p);

    graph compress_graph_LP(graph &initial_graph, unordered_map<edge,double> edges_scores, int k, vector<double> p,bool directed);

    void get_neighbors(uint32_t node, graph &g,unordered_map<int,unordered_set<uint32_t>> &neighbors,
                       int &maxDepth);
    graph Greedy_compression(graph &initial_graph,unordered_map<edge,double> edges_scores, int k, vector<double> p,bool directed);
    graph compress_graph_basic(graph &initial_graph, int k, vector<double> p,bool directed);

    graph random_compression_graph(graph &initial_graph,bool directed);

    vector<edge> perturbate_solution(vector<edge> &s);

    graph Simulated_annealing ( int max_iterations,
                                       double initial_temperature,
                                       double decrease_factor,
                                       graph &initial_graph,
                                       bool directed,
                                       int k,vector<double> p);







    tuple<double,graph> evaluate_permutation ( vector<edge> &permutation,bool directed, vector<double> &p,
                                  int k,graph &g);


    graph compress_graph_greedy(graph &initial_graph, int k, vector<double> p,bool directed);



    unordered_set<uint32_t> neighbors(graph &g,int max_depth,uint32_t u,uint32_t v);
    vector<edge> greedy_edges_order(graph &g,bool directed,int k);


}

#endif //P_K_COMPRESSION_P_K_COMPRESSION_H
